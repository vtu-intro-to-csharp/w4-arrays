﻿using System;

namespace ConsecutiveZeros
{
    class Program
    {
        //9.	Да се състави програма, която установява
        //има ли в редицата от цели числа а0, а1, …, аn-1  два последователни нулеви елемента.
        static void Main(string[] args)
        {
            Console.Write("Enter the length of the sequence: ");
            int length = int.Parse(Console.ReadLine());

            int[] numbers = new int[length];
            for (int i = 0; i < numbers.Length; i++)
            {
                Console.Write("Number " + (i + 1) + " = ");
                numbers[i] = int.Parse(Console.ReadLine());
            }

            bool isFound = false;
            for(int i = 1; i < numbers.Length; i++)
            {
                if (numbers[i - 1] == 0 && numbers[i] == 0)
                {
                    isFound = true;
                    break;
                }
            }

            if (isFound)
            {
                Console.WriteLine("There are two consecutive zeroes.");
            }
            else
            {
                Console.WriteLine("There are not two consecutive zeroes.");
            }
        }
    }
}
