﻿using System;

namespace AverageGrade
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the count of the student's grades: ");
            int count = int.Parse(Console.ReadLine());

            double[] grades = new double[count];

            for (int i = 0; i < grades.Length; i++)
            {
                Console.Write("Grade " + (i + 1) + " = ");
                grades[i] = double.Parse(Console.ReadLine());
            }

            double sum = 0;
            for (int i = 0; i < grades.Length; i++)
            {
                sum += grades[i];
            }

            //Console.WriteLine("The average grade is: " + sum / count);
            Console.WriteLine("The average grade is: " + Math.Round(sum / count, 2));
        }
    }
}
