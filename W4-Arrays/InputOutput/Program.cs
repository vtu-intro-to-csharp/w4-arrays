﻿using System;

namespace InputOutput
{
    class Program
    {
        static void Main(string[] args)
        {
            //input
            Console.Write("Enter the length of the array: ");
            int length = int.Parse(Console.ReadLine());

            int[] array = new int[length];
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write("array[" + i + "] = ");
                array[i] = int.Parse(Console.ReadLine());
            }

            //output
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine("array[" + i + "] = " + array[i]);
            }
        }
    }
}
