﻿using System;

namespace FindMax
{
    class Program
    {
        //6.	Да се състави програма,
        //която въвежда n на брой реални числа,
        //намира и извежда стойността и индекса най-голямото от тях
        static void Main(string[] args)
        {
            Console.Write("Enter numbers count: ");
            int n = int.Parse(Console.ReadLine());

            double[] numbers = new double[n];
            for(int i = 0; i < numbers.Length; i++)
            {
                Console.Write("Number " + (i + 1) + " = ");
                numbers[i] = double.Parse(Console.ReadLine());
            }

            double max = numbers[0];
            int maxIndex = 0;
            for(int i = 1; i < numbers.Length; i++)
            {
                if(numbers[i] > max)
                {
                    max = numbers[i];
                    maxIndex = i;
                }
            }

            Console.WriteLine("The biggest number is " + max + ". At index " + maxIndex);
        }
    }
}
