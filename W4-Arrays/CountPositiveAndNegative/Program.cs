﻿using System;

namespace CountPositiveAndNegative
{
    class Program
    {
        //4.	Да се състави програма,
        //която въвежда редица от n на брой цели числа а0, а1, …, аn-1.
        //Намерете и изведете броя на положителните и броя на отрицателните елементи на редицата.
        static void Main(string[] args)
        {
            Console.Write("Enter the count of the numbers: ");
            int count = int.Parse(Console.ReadLine());

            int[] numbers = new int[count];
            //input
            for (int i = 0; i < numbers.Length; i++)
            {
                Console.Write("a" + i + " = ");
                numbers[i] = int.Parse(Console.ReadLine());
            }

            //calculations
            int countPositive = 0;
            int countNegative = 0;
            //for(int i = 0; i < numbers.Length; i++)
            //{
            //    if(numbers[i] > 0)
            //    {
            //        countPositive++;
            //    }
            //    else if (numbers[i] < 0)
            //    {
            //        countNegative++;
            //    }                
            //}

            foreach(int number in numbers)
            {
                if(number > 0)
                {
                    countPositive++;
                }
                else if(number < 0)
                {
                    countNegative++;
                }
            }

            Console.WriteLine("Count of positive numbers: " + countPositive);
            Console.WriteLine("Count of negative numbers: " + countNegative);
        }
    }
}
