﻿using System;

namespace FindX
{
    class Program
    {
        //7.	Дадена е редицата от цели числа а0, а1, …, аn-1 (n >= 1) и цяло число x.
        //Да се състави програма, която намира колко пъти x се съдържа в редицата.
        static void Main(string[] args)
        {
            Console.Write("Enter the length of the sequence: ");
            int length = int.Parse(Console.ReadLine());

            int[] numbers = new int[length];
            for (int i = 0; i < numbers.Length; i++)
            {
                Console.Write("Number " + (i + 1) + " = ");
                numbers[i] = int.Parse(Console.ReadLine());
            }

            Console.Write("Enter a number to search for. X = ");
            int x = int.Parse(Console.ReadLine());
            int counter = 0;
            foreach(int number in numbers)
            {
                if (number == x)
                {
                    counter++;
                }
            }

            Console.WriteLine("X is found " + counter + " time(s).");
        }
    }
}
